<?php
/**
 * @author paulocarvalho
 */

namespace PauloCarvalho\Database;


class RecordMapper extends Mapper
{
  private $limit;
  private $where;
  private $select;
  private $group;
  private $order;

  /**
   * @param null $id
   *
   * @return mixed
   */
  public function find($id = null)
  {
    if($id)
    {
      $id = (int) $id;
      return  $this->mapperInstance()->object($id,get_called_class(),get_class_vars(get_called_class()));

    }
  }

  /**
   * @return mixed
   */
  public function many()
  {
    $limit = $this->limit;
    $where = $this->where;
    $select = $this->select;
    $group = $this->group;
    $order = $this->order;
    return $this->mapperInstance()->objects(get_called_class(),get_class_vars(get_called_class()), $limit, $where, $select, $group, $order);
  }

  /**
   * @param $value
   *
   * @return $this
   */
  public function limit($value)
  {
    $this->limit = $value;
    return $this;
  }

  /**
   * @param array $value  array of array values (field, condition, value)
   *
   * @return $this
   */
  public function where($value)
  {
    $this->where = $value;
    return $this;
  }

  /**
   * @param $value
   *
   * @return $this
   */
  public function select($value)
  {
    $this->select = $value;
    return $this;
  }

  /**
   * @param $value
   *
   * @return $this
   */
  public function group($value)
  {
    $this->group = $value;
    return $this;
  }

  /**
   * @param $value
   *
   * @return $this
   */
  public function order($value)
  {
    $this->order = $value;
    return $this;
  }

  /**
   * @param $key
   *
   * @return mixed
   */
  public function hasMany($key)
  {
    if(is_array($this->hasMany))
    {
      $obj = $this->mapperInstance()->related(
        $this->id,
        $key,
        $this->hasMany[1],
        get_class_vars(get_called_class())
      );
      return $obj;
   }
  }

  /**
   * @param $key
   *
   * @return mixed
   */
  public function belongsTo($key)
  {
    if(is_array($this->belongsTo))
    {
     $key = $key.'Id';
     $obj = new $this->belongsTo[1]();
     return $obj->find($this->{$key});
    }
  }

  /**
   * @return mixed
   */
  public function update()
  {
    return $this->mapperInstance()->updateObject($this);
  }

  /**
   * @return mixed
   */
  public function save(){
    return $this->mapperInstance()->insertObject($this);
  }

  /**
   *
   */
  public function delete(){
    $this->mapperInstance()->delete($this->getId(), $this->getTable());
  }

  /**
   * @return mixed
   */
  public function getTable()
  {
    return $this->table;
  }

  /**
   * @return mixed
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @return mixed
   */
  public function getPK()
  {
    return $this->pk;
  }

  /**
   * @return mixed
   */
  public function getTimeStamps()
  {
    return $this->timestamps;
  }

  /**
   *
   */
  public function removeAttributes(){
    unset($this->table);
    unset($this->pk);
    unset($this->belongsTo);
    unset($this->hasMany);
  }
}
