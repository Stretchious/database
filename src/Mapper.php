<?php
/**
 * @author paulocarvalho
 */
namespace PauloCarvalho\Database;
use Packaged\Config\Provider\Ini\IniConfigProvider;

class Mapper
{
  protected static $instance;
  protected static $config;


  private function setConfig()
  {
    $config = new IniConfigProvider('conf/'.getenv('cubex_env').'.ini');
    return $config->getSection('database');
  }

  private function setAdapter()
  {
    switch($this->setConfig()->getItem('DB_ADAPTER'))
    {
      case 'mysql':
        return  new MySqlAdapter($this->instance());
      break;
    }
  }

  private function instance()
  {
    return self::$instance = new Database($this->setConfig());
  }

  public function mapperInstance()
  {
    return $this->setAdapter();
  }

}
