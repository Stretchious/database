<?php
namespace PauloCarvalho\Database;

use PDO;

/**
 * @author Paulo.Carvalho
 */

class Database
{
  /* database Details */
  /**
   * @var string
   */
  private $host = '';
  /**
   * @var string
   */
  private $user = '';
  /**
   * @var string
   */
  private $pass = '';
  /**
   * @var string
   */
  private $dbname = '';
  /**
   * @var PDO|string
   */
  public $dbhost = '';
  /**
   * @var string
   */
  public $adapter = '';


  /**
   * @param null   $details
   */
  public function __construct($details = null){

    $this->host    = $details['DB_HOST'];
    $this->user    = $details['DB_USER'];
    $this->dbname  = $details['DB_NAME'];
    $this->pass    = $details['DB_PASS'];
    $this->adapter = $details['DB_ADAPTER'];

    switch($this->adapter)
    {
      case 'mysql' :
      {
        try
        {
          /*** connect to mySql database ***/
          $this->dbhost = new PDO('mysql:host='
                                  . $this->host . ';dbname='
                                  . $this->dbname,
                                  $this->user,
                                  $this->pass);
        }
        catch(PDOException $e){
          $this->error = $e->getMessage();
        }
        break;
      }
      case 'sqlite':
        {
          try {
            /*** connect to SQLite database ***/
            $this->dbhost = new PDO("sqlite:".$details['SQLITEDB'].".sdb");
            $sqlite = new SqliteAdapter($this->dbhost);
            $this->dbhost = $sqlite->dbhost;
          }
          catch(PDOException $e)
          {
            echo $e->getMessage();
          }
          break;
        }
    }
    $this->_init();
  }
  /**
   *
   */
  private function _init()
  {
    $this->dbhost->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }

  /**
   *
   */
  protected function beginTransaction()
  {
    $this->dbhost->beginTransaction();
  }

  /**
   *
   */
  protected function commitTransaction()
  {
    $this->dbhost->commit();
  }

  /**
   * @return string
   */
  protected function lastId()
  {
    return $this->dbhost->lastInsertId();
  }

  /**
   *
   */
  protected function rollback()
  {
    $this->dbhost->rollback();
  }

  /**
   *
   */
  protected function shutDown()
  {
    $this->dbhost = null;
  }
}
