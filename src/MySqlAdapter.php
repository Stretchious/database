<?php
namespace PauloCarvalho\Database;

use PDO;

/**
 * @author paulocarvalho
 */

class MySqlAdapter
{
  private $db;
  private $limit;
  public function __construct($db)
  {
    $this->db = $db->dbhost;
    return $this;
  }

  /**
   * @param $class
   * @param $props
   * @param $limit
   * @param $where
   * @param $select
   * @param $group
   *
   * @return mixed
   */
  public function objects($class, $props, $limit, $where, $select, $group, $order)
  {
    /*** limit ***/
    $limit = ($limit > 0) ? " LIMIT {$limit}" : "";

    /*** where ***/
    $where = (is_array($where) && count($where) > 0) ? $this->where($where) : "";

    /*** select ***/
    $select = (!empty($select)) ? $this->select($select) : "";

    /*** group ***/
    $group = (!empty($group)) ? $this->group($group) : "";

    /*** order ***/
    $order = (!empty($order)) ? $this->order($order) : "";

    /*** The SQL SELECT statement ***/
    $sql = "SELECT *{$select} FROM {$props['table']}{$where}{$group}{$order}{$limit}";

    /*** fetch into an PDOStatement object ***/
    $stmt = $this->db->query($sql);

    /*** fetch into the animals class ***/
    $objs = $stmt->fetchALL(PDO::FETCH_CLASS, $class);

    return $objs;
  }

  /**
   * @param $id
   * @param $class
   * @param $props
   *
   * @return mixed
   */
  public function object($id, $class, $props)
  {
    /*** prepare the SQL statement ***/
    $stmt =  $this->db->prepare("SELECT * FROM {$props['table']} WHERE {$props['pk']} = :{$props['pk']}");

    /*** bind the paramaters ***/
    $p =[':'.$props['pk'] => $id];

    /*** execute the prepared statement ***/
    $stmt->execute($p);

    /*** fetch into the animals class ***/
    $obj =  $stmt->fetchALL(PDO::FETCH_CLASS, $class);

    return $obj[0];
  }

  /**
   * @param $id
   * @param $relatedClass
   * @param $class
   * @param $props
   *
   * @return mixed
   */
  public function related($id, $relatedClass, $class, $props)
  {
    /*** prepare the SQL statement ***/
    $stmt =  $this->db->prepare("SELECT * FROM {$relatedClass} WHERE {$props['table']}Id = :{$props['pk']}");

    $p =[':'.$props['pk'] => $id];

    /*** execute the prepared statement ***/
    $stmt->execute($p);

    /*** fetch into the class ***/
    $obj =  $stmt->fetchALL(PDO::FETCH_CLASS, $class);


    return $obj;
  }

  /**
   * @param $id
   * @param $table
   */
  public function delete($id, $table)
  {
    $sql = "DELETE FROM {$table} WHERE id =  :id";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $stmt->execute();
  }

  /**
   * @param $obj
   *
   * @return mixed
   */
  public function insertObject($obj)
  {
    $table = $obj->getTable();
    $keys   = [];
    $values = [];
    $bind   = [];
    $obj->removeAttributes();

    $array = (array) $obj;

    if($obj->getTimeStamps())
    {
      $array['createdAt'] = date("Y-m-d H:i:s");
      $array['updatedAt'] = date("Y-m-d H:i:s");
    }

    foreach($array as $key => $value)
    {
      $keys[]   = $key;
      $values[':'.$key] = $value;
      $bind[] = ':'.$key;
    }
    array_shift($keys);
    array_shift($values);
    array_shift($bind);

    $sql = "INSERT INTO {$table} (".implode(',', $keys).") VALUES (".implode(', ', $bind).")";

    $stmt = $this->db->prepare($sql);

    $obj = $stmt->execute($values);

    //$obj = (object) $obj;
    $obj->id = $this->db->lastInsertId();
    return $obj;
  }

  /**
   * @param $obj
   *
   * @return mixed
   */
  public function updateObject($obj)
  {
    $id = $obj->getId();
    $table = $obj->getTable();
    $pk = $obj->getPK();
    $keys   = [];
    $values = [];
    $bind   = [];
    $obj->removeAttributes();
    if($obj->getTimeStamps())
    {

      $obj->updatedAt = date("Y-m-d H:i:s");
    }
    unset($obj->timestamps);
    foreach($obj as $key => $value)
    {
      if($obj->{$value} !== $value)
      {
        $values[] = $value;
      }
      $keys[]   = $key;

      $bind[] =  $key.' = ?';
    }
    array_shift($keys);
    array_shift($values);
    array_shift($bind);

    $sql = "UPDATE {$table} SET ". implode(", ",$bind) . " WHERE {$pk} = {$id}";
    $stmt = $this->db->prepare($sql);
    $stmt->execute($values);
    return $obj;
  }

  /**
   * @param $sql
   *
   * @return mixed
   */
  public function run($sql)
  {
    $stmt = $this->db->query($sql);
    $result =  $stmt->fetch(PDO::FETCH_BOTH);
    return $result;
  }

  /**
   * @param $where
   *
   * @return string
   */
  private function where($where)
  {
    $stmt = '';
    $count = 1;

    if(!is_array($where))
    {
      return $stmt;
    }

    foreach($where as $v)
    {
      if(!is_array($v) || count($v) != 3)
      {
        continue;
      }
      $stmt .= ($count == 1) ? " WHERE " : " AND ";
      $stmt .= "{$v[0]} {$v[1]} {$v[2]}";
      $count++;
    }

    return $stmt;
  }

  /**
   * @param $select
   *
   * @return string
   */
  private function select($select)
  {
    $stmt = '';

    if(is_array($select))
    {
      foreach($select as $v)
      {
        $stmt .= ',' . $v;
      }
    }
    else
    {
      $stmt .= ',' . $select;
    }

    return $stmt;
  }

  /**
   * @param $group
   *
   * @return string
   */
  private function group($group)
  {
    $stmt = '';
    $count = 1;

    if(is_array($group))
    {
      foreach($group as $v)
      {
        $stmt .= ($count == 1) ? " GROUP BY " : ", ";
        $stmt .= $v;
        $count++;
      }
    }
    else
    {
      $stmt .= " GROUP BY " . $group;
    }

    return $stmt;
  }

  /**
   * @param $order
   *
   * @return string
   */
  private function order($order)
  {
    $stmt = '';
    $count = 1;

    if(is_array($order))
    {
      foreach($order as $v)
      {
        $stmt .= ($count == 1) ? " ORDER BY " : ", ";

        if(!is_array($v))
        {
          $stmt .= $v . ' ASC';
        }
        else
        {
          $stmt .= "{$v[0]} {$v[1]}";
        }
        $count++;
      }
    }
    else
    {
      $stmt .= " ORDER BY " . $order . " ASC";
    }

    return $stmt;
  }
}
