<?php
/**
 * @author paulocarvalho
 */
namespace PauloCarvalho\Database;

class DBQuery extends Mapper
{
  public  $table;
  public  $where;
  public  $limit;
  public  $query;
  public  $join;
  public  $select;
  public  $db;
  public $buildQuery;

  public function __construct()
  {

  }

  public function table($table)
  {
    $this->table = $table;
    return $this;
  }

  public function where($values)
  {
    $this->where = 'WHERE ';
    $this->where .= $values;
    return $this;
  }

  public function limit($value)
  {
    $this->limit = $value;
    return $this;
  }

  public function select($values)
  {
    $this->select = 'SELECT ';
    $this->select .= $values;
    $this->select .= ' FROM '.$this->table. ' ';
    return $this;
  }

  public function query($sql)
  {
    $this->query = $sql;
    return $this;
  }

  private function buildQuery()
  {
    $this->buildQuery = $this->select;
    return $this;
  }

  public function run()
  {
    if(!empty($this->query))
    {
      $result =  $this->mapperInstance()->run($this->query);
    }
    else
    {
      $this->buildQuery();
      $result =  $this->mapperInstance()->run($this->buildQuery);
    }

    return $result;
  }
}
